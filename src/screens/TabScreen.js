import React, { Component } from 'react';
import { Container, Header, Content, Left, Body, Right, Title, Tab, Tabs } from 'native-base';
import { Appbar, Text } from 'react-native-paper';

import Tab1 from './tabs/Tab1';
import Tab2 from './tabs/Tab2';
import Tab3 from './tabs/Tab3';
export default class TabsExample extends Component {
  render() {
    return (
      <Container>
        <Appbar style={{ backgroundColor: "#424242", alignContent: "center", justifyContent: "center" }}>
          <Text style={{ color: "white", fontSize: 18 }}>NEWS ONLINE</Text>
        </Appbar>

        <Tabs tabBarUnderlineStyle={{ backgroundColor: '#43A047' }}>
          <Tab tabStyle={{ backgroundColor: '#424242' }} activeTabStyle={{ backgroundColor: '#616161' }} textStyle={{ color: 'white' }} activeTextStyle={{ color: 'white' }} heading="General">
            <Tab1 />
          </Tab>
          <Tab tabStyle={{ backgroundColor: '#424242' }} activeTabStyle={{ backgroundColor: '#616161' }} textStyle={{ color: 'white' }} activeTextStyle={{ color: 'white' }} heading="Business">
            <Tab2 />
          </Tab>
          <Tab tabStyle={{ backgroundColor: '#424242' }} activeTabStyle={{ backgroundColor: '#616161' }} textStyle={{ color: 'white' }} activeTextStyle={{ color: 'white' }} heading="Technology">
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
    );
  }
}